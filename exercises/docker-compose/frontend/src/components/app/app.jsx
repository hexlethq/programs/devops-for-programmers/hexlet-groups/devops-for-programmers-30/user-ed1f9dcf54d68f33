import React, {useEffect, useState} from 'react';
import axios from 'axios';

function App() {
  const [postgresText, setPostgresText] = useState('');

  useEffect(() => {
    axios.get('/api/hello-world')
      .then(({data}) => setPostgresText(data))
      .catch(() => console.log('test'));
  },[]);

  return <>
    <h1 style={{textAlign: 'center', color: 'Blue'}}>Hexlet project</h1>
    <h2 style={{textAlign: 'center', color: '#333'}}>{postgresText} in React App</h2>
    </>;
}

export default App;

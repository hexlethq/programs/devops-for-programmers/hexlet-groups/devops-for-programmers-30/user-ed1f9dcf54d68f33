import express from 'express';
import pgPromise from 'pg-promise';

const app = express();
const pgp = pgPromise();

const {
  POSTGRES_HOST: host,
  POSTGRES_PORT: port,
  POSTGRES_USER: user,
  POSTGRES_PASSWORD: password,
  POSTGRES_DB: database,
  PORT: apiPort,
} = process.env;
const db = pgp(`postgres://${user}:${password}@${host}:${port}/${database}`);

app.get('/hello-world', (req, res) => {
  db.one('SELECT $1 AS value', 'Retrieved text from postgres')
    .then((data) => res.send(data.value))
    .catch((error) => res.send(error));
});

const startServer = () => {
  app.listen(apiPort, () => {
    console.log(`Server is up on port ${apiPort}`);
  });
};

startServer();

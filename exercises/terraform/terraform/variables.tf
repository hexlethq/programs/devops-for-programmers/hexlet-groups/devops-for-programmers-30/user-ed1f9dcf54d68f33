// Токен DO и путь к приватному ключу, будут передаваться через CLI
variable "do_token" {
  description = "Please Enter DO Token"
  type        = string
}

variable "ssh_name" {
  type        = string
  default     = "Mac Pro (ed25519)"
}

variable "environment" {
  type        = string
  default     = "development"
}

variable "region" {
  type        = string
  default     = "ams3"
}

variable "droplet_image" {
  type        = string
  default     = "docker-20-04"
}

variable "droplet_size" {
  type        = string
  default     = "s-1vcpu-1gb"
}

variable "entry_port" {
  type        = string
  default     = "80"
}

variable "target_port" {
  type        = string
  default     = "5000"
}

variable "domain" {
  type        = string
  default     = "hexlet-project.xyz"
}

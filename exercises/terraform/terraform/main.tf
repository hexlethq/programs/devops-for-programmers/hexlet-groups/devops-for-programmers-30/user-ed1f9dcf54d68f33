// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs
terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "2.16.0"
    }
  }
}

provider "digitalocean" {
  // Использование переменной (токен доступа к DO)
  // https://www.terraform.io/docs/language/values/variables.html
  token = var.do_token
}

// Ключ можно либо получить созданный, либо создать новый
// resource "digitalocean_ssh_key" "default" {
//   name       = "Terraform Homework"
//   public_key = file("~/.ssh/id_rsa.pub")
// }
// Используется data source - ресурс не создаётся. Terraform запрашивает информацию о ресурсе
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/data-sources/droplet
data "digitalocean_ssh_key" "my_ssh" {
  // Имя под которым ключ сохранён в DO
  // https://cloud.digitalocean.com/account/security
  name = var.ssh_name
}

// Создаём дроплет
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/droplet
resource "digitalocean_droplet" "web-terraform-homework-01" {
  image  = var.droplet_image
  name   = "web-terraform-homework-01"
  region = var.region
  size   = var.droplet_size
  tags    = [var.environment]

  // Добавление приватного ключа на создаваемый сервер
  // Обращение к datasource выполняется через data.
  ssh_keys = [
    data.digitalocean_ssh_key.my_ssh.id
  ]
}

resource "digitalocean_droplet" "web-terraform-homework-02" {
  image  = var.droplet_image
  name   = "web-terraform-homework-02"
  region = var.region
  size   = var.droplet_size
  tags    = [var.environment]

  // Добавление приватного ключа на создаваемый сервер
  // Обращение к datasource выполняется через data.
  ssh_keys = [
    data.digitalocean_ssh_key.my_ssh.id
  ]
}

// Создание балансировщика нагрузки
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/loadbalancer
resource "digitalocean_loadbalancer" "public" {
  name = "web-terraform-homework-balancer"
  region = var.region

  forwarding_rule {
    // Порт по которому балансировщик принимает запросы
    entry_port = var.entry_port
    entry_protocol = "http"

    // Порт по которому балансировщик передает запросы (на другие сервера)
    target_port = var.target_port
    target_protocol = "http"
  }

  // Порт, протокол, путь по которому балансировщик проверяет, что дроплет живой и принимает запросы
  healthcheck {
    port = var.target_port
    protocol = "http"
    path = "/"
  }

  droplet_ids = [
    digitalocean_droplet.web-terraform-homework-01.id,
    digitalocean_droplet.web-terraform-homework-02.id
  ]
}

// Создание домена
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/domain
resource "digitalocean_domain" "domain" {
  name = var.domain
  ip_address = digitalocean_loadbalancer.public.ip
}

// Outputs похожи на возвращаемые значения. Они позволяют сгруппировать информацию или распечатать то, что нам необходимо
// https://www.terraform.io/docs/language/values/outputs.html
output "droplets_ips" {
  // Обращение к ресурсу. Каждый ресурс имеет атрибуты, ккоторым можно получить доступ
  value = [
    digitalocean_droplet.web-terraform-homework-01.ipv4_address,
    digitalocean_droplet.web-terraform-homework-02.ipv4_address
  ]
}

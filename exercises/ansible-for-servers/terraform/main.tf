// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs
terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "2.16.0"
    }
  }
}

provider "digitalocean" {
  // Использование переменной (токен доступа к DO)
  // https://www.terraform.io/docs/language/values/variables.html
  token = var.do_token
}

// Ключ можно либо получить созданный, либо создать новый
// resource "digitalocean_ssh_key" "default" {
//   name       = "Terraform Homework"
//   public_key = file("~/.ssh/id_rsa.pub")
// }
// Используется data source - ресурс не создаётся. Terraform запрашивает информацию о ресурсе
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/data-sources/droplet
data "digitalocean_ssh_key" "my_ssh" {
  // Имя под которым ключ сохранён в DO
  // https://cloud.digitalocean.com/account/security
  name = var.ssh_name
}

resource "digitalocean_vpc" "example" {
  name     = "example-project-network"
  region   = var.region
  ip_range = "10.10.10.0/24"
}

// Outputs похожи на возвращаемые значения. Они позволяют сгруппировать информацию или распечатать то, что нам необходимо
// https://www.terraform.io/docs/language/values/outputs.html
output "droplets" {
  // Обращение к ресурсу. Каждый ресурс имеет атрибуты, к которым можно получить доступ
  value = [
    digitalocean_droplet.web-1,
    digitalocean_droplet.web-2
  ]
}

// Создание балансировщика нагрузки
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/loadbalancer
resource "digitalocean_loadbalancer" "lb" {
  name = "lb"
  region = var.region
  vpc_uuid = digitalocean_vpc.example.id

  forwarding_rule {
    // Порт по которому балансировщик принимает запросы
    entry_port = var.entry_port
    entry_protocol = "http"

    // Порт по которому балансировщик передает запросы (на другие сервера)
    target_port = var.target_port
    target_protocol = "http"
  }

  // Порт, протокол, путь по которому балансировщик проверяет, что дроплет жив и принимает запросы
  healthcheck {
    port = var.target_port
    protocol = "http"
    path = "/"
  }

  droplet_ids = [
    digitalocean_droplet.web-1.id,
    digitalocean_droplet.web-2.id
  ]
}

// Создание домена
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/domain
resource "digitalocean_domain" "domain" {
  name = var.domain
  ip_address = digitalocean_loadbalancer.lb.ip
}

// Создаём дроплет
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/droplet
resource "digitalocean_droplet" "web-1" {
  image  = var.droplet_image
  name   = "web-1"
  region = var.region
  size   = var.droplet_size
  tags    = [var.environment]
  vpc_uuid = digitalocean_vpc.example.id

  // Добавление приватного ключа на создаваемый сервер
  // Обращение к datasource выполняется через data.
  ssh_keys = [
    data.digitalocean_ssh_key.my_ssh.id
  ]

  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.pvt_key)
    timeout = "2m"
  }
}

resource "digitalocean_droplet" "web-2" {
  image  = var.droplet_image
  name   = "web-2"
  region = var.region
  size   = var.droplet_size
  tags    = [var.environment]
  vpc_uuid = digitalocean_vpc.example.id

  // Добавление приватного ключа на создаваемый сервер
  // Обращение к datasource выполняется через data.
  ssh_keys = [
    data.digitalocean_ssh_key.my_ssh.id
  ]

  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.pvt_key)
    timeout = "2m"
  }
}
